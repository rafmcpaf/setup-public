#!/bin/bash
sudo apt-get update
sudo apt-get install -y git ansible curl

if [[ ! -f ~/.ssh/id_rsa ]]; then
  cat /dev/zero | ssh-keygen -t rsa -b 4096 -C "$USER" -q -N ""
fi

pubkey=`cat ~/.ssh/id_rsa.pub`
curl -u stijn@vandenbussche.xyz https://api.bitbucket.org/2.0/users/%7B23894471-03e9-4c48-921a-d14bb6192276%7D/ssh-keys -X POST -d "{\"key\":\"$pubkey\"}" -H "Content-Type: application/json"

mkdir -p ~/projects
cd ~/projects
if [[ ! -d ~/projects/setup-rafmcpaf ]]; then
  git clone git@bitbucket.org:rafmcpaf/setup-rafmcpaf.git
fi
